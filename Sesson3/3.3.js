function CharFreq(str){
  let str1=str.split(",").map(item=>(parseInt(item)));
  let Numbers=[];
  let NumbersF=[];
  let count=0;
  let numMax=0
  let num=0
  for(let i=0;i<str1.length;i++){
    if(Numbers.includes(str1[i])==false){
      Numbers.push(str1[i])
    }  
  }
  
  for(let i=0;i<Numbers.length;i++){
    for(let j=0;j<str1.length;j++){
      if(Numbers[i]==str1[j]){
        count+=1
    }  
    }
    NumbersF.push(count)
    count=0
    
  }
  numMax=Math.max(...NumbersF)
  num=NumbersF.indexOf(numMax)
  
  return{
    
   n:numMax,
   p:Numbers[num]
  
  }
  
}


var str=prompt("enter comma seperated numbers ")
var answer=CharFreq(str)
console.log("The number "+answer.p+" appears "+answer.n+" more times than any other element in the array.")
