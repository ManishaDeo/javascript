let companies={
    "TCS": {
    "revenue" : 1000000,
    "expenses": { "salaries": 30, 
    "rent": 20,
    "utilites": 15
    },
    "employees": [
    {
    "name": "Joe",
    "age" : 30,
    "role" : "Admin"
    },
    {
    "name": "34",
    "age" : 40,
    "role" :"Tester"
    },
    {
    "name": "Sherlock",
    "age" : 45,
    "role" : "Programmer"
    }
    ]
},
"Osmosys": {
    "revenue" : 5000000,
    "expenses": { "salaries": 30, 
    "rent": 20,
    "utilites": 15
    },
    "employees": [
    {
    "name": "Max",
    "age" : 30,
    "role" : "Admin"
    },
    {
    "name": "Luke",
    "age" : 40,
    "role" :"Tester"
    },
    {
    "name": "Thomas",
    "age" : 45,
    "role" : "Programmer"
    }
    ]
}
}

let youngest_employee=function(company){
    let young_age=50;
    let young_name;
    for(let i=0;i<companies[company]["employees"].length;i++){
        if(companies[company]["employees"][i]['age']<young_age){
            young_age=companies[company]["employees"][i]['age'];
            young_name=companies[company]["employees"][i]['name'];
        }
}
console.log(young_name+" is the youngest man in "+ company +" who is "+young_age+" years old");
}

let oldest_employee=function(company){
    let old_age=0;
    let old_name;
    for(let i=0;i<companies[company]["employees"].length;i++){
        if(companies[company]["employees"][i]['age']>old_age){
            old_age=companies[company]["employees"][i]['age'];
            old_name=companies[company]["employees"][i]['name'];
        }
}
console.log(old_name+" is the oldest man in "+ company +" who is "+old_age+" years old");
}

let age_filter=function(range_age){
    let a;
    a=companies[company]["employees"].filter(r=> r.age>=range_age);
    console.log("The employees whose age is greater than or equal to "+range_age+" : ");
    for(let i=0;i<a.length;i++){
        console.log(a[i]["name"]);
    }
}

let profit=function(company){
    let total=companies[company]["expenses"]['rent']+companies["TCS"]["expenses"]['salaries']+companies["TCS"]["expenses"]["utilites"]
    let revenue=companies[company]["revenue"]
    let profit=revenue-(revenue*total*0.01);
    console.log("The profit of "+company+" : "+profit);
}

let roles=function(company){
    let req_roles=prompt("Enter the roles :'Programmer' 'Tester' 'Admin' ");
    let a;
    a=companies[company]["employees"].filter(r=> r.role==req_roles);
    console.log("The "+req_roles+"'s are ");
    for(let i=0;i<a.length;i++){
        console.log(a[i]["name"]);
    }
    
}



